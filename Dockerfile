FROM golang:alpine AS builder
MAINTAINER Eugene Klimov <bloodjazman@gmail.com>

WORKDIR /metrika2clickhouse/

ENV GOPATH /metrika2clickhouse/

COPY ./.gometalinter.json /metrika2clickhouse/
COPY ./ /metrika2clickhouse/src/bitbucket.org/clickhouse_pro/metrika2clickhouse/
COPY ./id_rsa /root/.ssh/id_rsa
RUN chmod 0600 /root/.ssh/id_rsa

RUN apk add --no-cache --update git openssh-client
RUN touch /root/.ssh/known_hosts
RUN ssh-keygen -R github.com
RUN ssh-keygen -R bitbucket.org
RUN ssh-keyscan -H github.com >> /root/.ssh/known_hosts
RUN ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts
RUN git config --global url."git@github.com:".insteadOf "https://github.com/"
RUN git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"


RUN mkdir -p /metrika2clickhouse/bin
RUN cd /metrika2clickhouse/src/bitbucket.org/clickhouse_pro/metrika2clickhouse && go get -v ./...

RUN go get -u github.com/alecthomas/gometalinter
RUN /metrika2clickhouse/bin/gometalinter --install
RUN /metrika2clickhouse/bin/gometalinter --config=/metrika2clickhouse/.gometalinter.json --exclude=/go/src/ /metrika2clickhouse/src/bitbucket.org/clickhouse_pro/metrika2clickhouse/metrika2clickhouse/...

RUN go test -v bitbucket.org/clickhouse_pro/metrika2clickhouse/metrika2clickhouse \
    && go build -o /metrika2clickhouse/bin/metrika2clickhouse /metrika2clickhouse/src/bitbucket.org/clickhouse_pro/metrika2clickhouse/main.go \
    && rm -rf /metrika2clickhouse/src


FROM alpine:latest
ENV DOCKERIZE_VERSION v0.4.0
RUN apk add --no-cache --update ca-certificates openssl \
		&& apk add --no-cache --update gzip sed \
    && update-ca-certificates \
    && wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

COPY --from=builder /metrika2clickhouse/bin/metrika2clickhouse /metrika2clickhouse/bin/
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /usr/local/go/lib/time/zoneinfo.zip
ENTRYPOINT ["/bin/sh","-c"]