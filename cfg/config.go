package cfg

import (
	"bitbucket.org/clickhouse_pro/components/cfg"
	"flag"
)

//ConfigType inherited from bitbucket.org/clickhouse_pro/components/cfg
type ConfigType struct {
	cfg.ConfigType     `yaml:",inline"`
	Metrika2Clickhouse struct {
		MetrikaParallelJob    uint `yaml:"metrika_parallel_job"`
		AppMetrikaParallelJob uint `yaml:"appmetrika_parallel_job"`
		LoadData              bool `yaml:"load_data"`
	} `yaml:"metrika2clickhouse"`
}

//NewConfig initialize empty ConfigType structure
func NewConfig() *ConfigType {
	return &ConfigType{}
}

//DefineCLIFlags override bitbucket.org/clickhouse_pro/components/cfg ConfigType.DefineCLIFlags()
func (c *ConfigType) DefineCLIFlags() {
	c.ConfigType.DefineCLIFlags()
	flag.BoolVar(&c.Metrika2Clickhouse.LoadData, "metrika2clickhouse.loaddata", true, "you can skip loaddata phase, use it for")
	flag.UintVar(&c.Metrika2Clickhouse.MetrikaParallelJob, "metrika2clickhouse.metrika_parallel_job", 3, "you can parellel loading data but see quotas on https://tech.yandex.ru/metrika/doc/api2/logs/intro-docpage/")
	flag.UintVar(&c.Metrika2Clickhouse.AppMetrikaParallelJob, "metrika2clickhouse.appmetrika_parallel_job", 2, "you can parellel loading data but see quotas on https://tech.yandex.ru/appmetrica/doc/mobile-api/logs/quotas-docpage/")
}
