#!/bin/bash
set -xeuo pipefail
echo 1 > /proc/sys/vm/drop_caches
docker login -u clickhousepro cloud.canister.io:5000
docker-compose build metrika2clickhouse
docker-compose up -d metrika2clickhouse
sleep 1
WAIT="0"
while [ $(docker-compose ps | grep metrika2clickhouse | wc -l) = "0" ]; do
	echo "wait metrika2clickhouse running..."
	sleep 1
	WAIT=$[$WAIT + 1]
	if ["$WAIT" > "20"]; then
		echo "TIMEOUT"
		echo "docker publishing failed"
		exit 1
	fi
done
if [ $(docker-compose ps | grep metrika2clickhouse | grep Exit | wc -l) != "0" ]; then
	docker-compose logs metrika2clickhouse
	exit 1
fi

docker-compose down
docker-compose push metrika2clickhouse
echo "docker publishing done"
