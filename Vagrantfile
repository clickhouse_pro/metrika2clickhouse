# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure(2) do |config|
	config.vm.box = "ubuntu/xenial64"
	config.vm.box_check_update = false

	config.hostmanager.enabled = true
	config.hostmanager.manage_host = true
	config.hostmanager.ignore_private_ip = false
	config.hostmanager.include_offline = false

	config.vm.provider "virtualbox" do |vb|
		vb.gui = false
		vb.memory = "2048"
		vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant", "1"]
	end

	config.vm.define :metrika2clickhouse_develop do |metrika2clickhouse_develop|
    metrika2clickhouse_develop.vm.synced_folder "./", "/home/ubuntu/go/src/bitbucket.org/clickhouse_pro/metrika2clickhouse"
    metrika2clickhouse_develop.vm.synced_folder "../components", "/home/ubuntu/go/src/bitbucket.org/clickhouse_pro/components"
		metrika2clickhouse_develop.vm.host_name = "vagrant-metrika2clickhouse-clickhouse-pro"
		metrika2clickhouse_develop.hostmanager.aliases = ["vagrant.metrika2clickhouse.clickhouse.pro"]
		metrika2clickhouse_develop.vm.network "private_network", ip: "172.16.2.77"
		metrika2clickhouse_develop.vm.provision "shell", inline: <<-SHELL
			set -xeuo pipefail
			echo 'APT::Periodic::Enable "0";' > /etc/apt/apt.conf.d/02periodic
			export DEBIAN_FRONTEND=noninteractive
			export GOPATH=/home/ubuntu/go
			grep -q -F 'export GOPATH=$GOPATH' /home/ubuntu/.bashrc  || echo "export GOPATH=$GOPATH" >> /home/ubuntu/.bashrc
			grep -q -F 'export GOPATH=$GOPATH' /home/vagrant/.bashrc || echo "export GOPATH=$GOPATH" >> /home/vagrant/.bashrc
			grep -q -F 'export GOPATH=$GOPATH' /root/.bashrc         || echo "export GOPATH=$GOPATH" >> /root/.bashrc
			apt-get update
			apt-get install -y apt-transport-https software-properties-common aptitude
			# clickhouse
			apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E0C56BD4
			# gophers
			apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 136221EE520DDFAF0A905689B9316A7BC7917B12
			# docker
			apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8D81803C0EBFCD88
			add-apt-repository "deb https://download.docker.com/linux/ubuntu xenial edge"
			add-apt-repository "deb http://repo.yandex.ru/clickhouse/xenial/ stable main"
			add-apt-repository ppa:gophers/archive
			apt-get update
			apt-get install -y golang-1.9
			apt-get install -y docker-ce
			apt-get install -y htop ethtool mc iotop
			apt-get install -y python-pip
			apt-get install -y clickhouse-server-common clickhouse-client
			cp -fv /vagrant/deployment/vagrant/clickhouse/*.xml /etc/clickhouse-server/
			pip install -U docker-compose
			ln -nvsf /usr/lib/go-1.9/bin/go /bin/go
			ln -nvsf /usr/lib/go-1.9/bin/gofmt /bin/gofmt
			systemctl restart clickhouse-server

			mkdir -p -m 0700 /root/.ssh/
			cp -fv /vagrant/id_rsa /root/.ssh/id_rsa
			chmod 0600 /root/.ssh/id_rsa
			touch /root/.ssh/known_hosts
			ssh-keygen -R github.com
			ssh-keygen -R bitbucket.org
			ssh-keyscan -H github.com >> /root/.ssh/known_hosts
			ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts

			git config --global url."git@github.com:".insteadOf "https://github.com/"
			git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"
			bash -c "cd /home/ubuntu/go/src/bitbucket.org/clickhouse_pro/metrika2clickhouse && go get -v ./..."

			set +x

			echo "metrika2clickhouse_develop PROVISIONING DONE, use folloding scenario for developing"
			echo "#  vagrant ssh metrika2clickhouse_develop"
			echo "#  sudo bash -c \\\"cd /vagrant && GOPATH=/home/ubuntu/go go run main.go \\\""
			echo "for docker build run following command"
			echo "#  cd /vagrant && sudo ./docker-publisher.sh"
			echo "Good Luck ;)"
		SHELL
	end

end
