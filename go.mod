module bitbucket.org/clickhouse_pro/metrika2clickhouse

go 1.13

require (
	bitbucket.org/clickhouse_pro/components v0.0.0-20180208070017-8c55b42c9de7
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/jinzhu/configor v1.1.1 // indirect
	github.com/kshvakov/clickhouse v1.3.11 // indirect
	github.com/levigross/grequests v0.0.0-20190908174114-253788527a1a
	github.com/rs/zerolog v1.15.0
	github.com/samuel/go-zookeeper v0.0.0-20190923202752-2cc03de413da // indirect
	golang.org/x/net v0.0.0-20191011234655-491137f69257 // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45 // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/pool.v3 v3.1.1
	gopkg.in/yaml.v2 v2.2.4 // indirect
)
