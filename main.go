package main

import (
	"flag"

	"bitbucket.org/clickhouse_pro/components/chpool"
	"bitbucket.org/clickhouse_pro/components/zkpool"
	"github.com/rs/zerolog/log"

	basecfg "bitbucket.org/clickhouse_pro/components/cfg"
	"bitbucket.org/clickhouse_pro/metrika2clickhouse/cfg"
	"bitbucket.org/clickhouse_pro/metrika2clickhouse/metrika2clickhouse"
)

func main() {
	config := cfg.NewConfig()
	basecfg.InitConfig(config)
	zkpool.InitZookeeper(&config.ConfigType)
	chpool.InitClickhousePool(&config.ConfigType)
	chpool.DetectAllowDistributed(&config.ConfigType)
	chpool.DetectAllowReplicated(&config.ConfigType)
	chpool.CreateClickhouseDatabase(&config.ConfigType)
	chpool.CreateClickhouseTables(&config.ConfigType, metrika2clickhouse.TableDefinitions)
	if config.Metrika2Clickhouse.LoadData {
		dataLoder := metrika2clickhouse.NewDataLoader(config)
		dataLoder.LoadAllData()
	} else {
		flag.Usage()
		log.Fatal().Bool("config.Metrika2Clickhouse.LoadData", config.Metrika2Clickhouse.LoadData).Msg("Please set -loaddata.loaddata in commandline parameters")
	}
}
