package metrika2clickhouse

import (
	"fmt"
	"path"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	basecfg "bitbucket.org/clickhouse_pro/components/cfg"
	"bitbucket.org/clickhouse_pro/components/chpool"
	"bitbucket.org/clickhouse_pro/components/helpers"
	"bitbucket.org/clickhouse_pro/metrika2clickhouse/cfg"

	"github.com/levigross/grequests"
	"github.com/rs/zerolog/log"
	"gopkg.in/go-playground/pool.v3"
)

//DataLoader main metrika2clickhouse data structure
type DataLoader struct {
	loadAppMetrikaGoRoutinesPool pool.Pool
	loadMetrikaGoRoutinesPool    pool.Pool
	dispatchData                 *sync.WaitGroup
	config                       *cfg.ConfigType
}

//NewDataLoader DataLoader constructor, initialize limited pool of goroutines
func NewDataLoader(config *cfg.ConfigType) *DataLoader {
	d := DataLoader{
		config: config,
		loadAppMetrikaGoRoutinesPool: pool.NewLimited(config.Metrika2Clickhouse.AppMetrikaParallelJob),
		loadMetrikaGoRoutinesPool:    pool.NewLimited(config.Metrika2Clickhouse.MetrikaParallelJob),
		dispatchData:                 &sync.WaitGroup{},
	}
	return &d
}

type logRequestEvaluationJSON struct {
	LogRequestEvaluation struct {
		Possible               bool  `json:"possible"`
		MaxPossibleDayQuantity int64 `json:"max_possible_day_quantity"`
	} `json:"log_request_evaluation"`
}

type logRequestJSON struct {
	LogRequest struct {
		Parts []struct {
			Size       int `json:"size"`
			PartNumber int `json:"part_number"`
		} `json:"parts"`
		RequestID int      `json:"request_id"`
		CounterID int      `json:"counter_id"`
		Source    string   `json:"source"`
		Date1     string   `json:"date1"`
		Date2     string   `json:"date2"`
		Fields    []string `json:"fields"`
		Status    string   `json:"status"`
		Size      int      `json:"size"`
	} `json:"log_request"`
}

//LoadAllData load AppMetrika and Metrika data both use d.config settings
func (d *DataLoader) LoadAllData() {

	defer d.loadAppMetrikaGoRoutinesPool.Close()
	defer d.loadMetrikaGoRoutinesPool.Close()
	defer d.dispatchData.Wait()
	defer log.Info().Msg("LoadAllData DONE")

	appMetrikaBatch := d.loadAppMetrikaGoRoutinesPool.Batch()
	metrikaBatch := d.loadMetrikaGoRoutinesPool.Batch()

	go func() {
		for table := range TableDefinitions {
			if len(table) > 8 && table[:8] == "metrika_" {
				for _, counterID := range d.config.Metrika.CounterIds {
					metrikaBatch.Queue(d.metricaLoadData(table, table[8:], counterID))
				}
			} else if len(table) > 11 && table[:11] == "appmetrika_" {
				for _, applicationID := range d.config.AppMetrika.ApplicationIds {
					appMetrikaBatch.Queue(d.appMetricaLoadData(table, table[11:], applicationID))
				}
			}
		}
		appMetrikaBatch.QueueComplete()
		metrikaBatch.QueueComplete()
	}()

	appMetrikaBatch.WaitAll()
	metrikaBatch.WaitAll()

}

// @TODO 16h/DEV i need learn how refatoring with low cyclomatic complexity
//nolint: gocyclo
func (d *DataLoader) appMetricaLoadData(fullTable string, shortTable string, applicationID string) pool.WorkFunc {

	prepareRequestOptions := func() (baseURL, sqlFields string, reqOptions *grequests.RequestOptions, err error) {
		baseURL = fmt.Sprintf("%s/logs/v1/export/%s", basecfg.AppmetrikaURL, shortTable)
		reqFields, sqlFields := d.prepareFieldsToRequest(fullTable)
		dateSince, dateUntil := d.prepareDateRange(fullTable, applicationID)
		if dateSince.Equal(dateUntil) || dateSince.After(dateUntil) {
			return "", "", nil, fmt.Errorf("dateSince=%s >= dateUntil=%s", dateSince, dateUntil)
		}

		dateDimension := "receive"
		if strings.Contains(shortTable, "postbacks") {
			dateDimension = "default"
		}

		reqOptions = &grequests.RequestOptions{
			Params: map[string]string{
				"oauth_token":    d.config.OAuth.Token,
				"date_since":     dateSince.Format("2006-01-02 15:04:05"),
				"date_until":     dateUntil.Format("2006-01-02 15:04:05"),
				"date_dimension": dateDimension,
				"application_id": applicationID,
				"fields":         reqFields,
			},
			Headers: map[string]string{
				"Accept-Encoding": "gzip",
			},
		}
		return baseURL, sqlFields, reqOptions, nil
	}

	calculateAppmetrikaParts := func(baseURL string, reqOptions *grequests.RequestOptions) int {
		resp, err := grequests.Get(baseURL+".parts", reqOptions)
		log.Debug().Msgf("GET URL=%v Params=%v", baseURL+".parts", reqOptions.Params)
		if err != nil || !resp.Ok {
			log.Fatal().Int("response_status", resp.StatusCode).Str("response_body", resp.String()).Err(err).Msgf("%s Response error", baseURL+".parts")
		}
		log.Debug().Int("response_status", resp.StatusCode).Str("response_body", resp.String()).Msg("URL=" + baseURL + ".parts")
		parts, err := strconv.ParseInt(resp.String(), 10, 32)
		if err != nil {
			parts = 1
		}
		log.Debug().Int64("parts", parts).Err(err)
		return int(parts)
	}

	return func(wu pool.WorkUnit) (interface{}, error) {
		defer log.Info().Msgf("appMetricaLoadData %s end", shortTable)
		log.Info().Msgf("appMetricaLoadData %s begin", shortTable)

		if wu.IsCancelled() {
			return nil, nil
		}
		baseURL, sqlFields, reqOptions, err := prepareRequestOptions()
		if err != nil {
			return nil, err
		}

		parts := calculateAppmetrikaParts(baseURL, reqOptions)
		reqOptions.Params["parts"] = strconv.Itoa(parts)

		for part := 0; part < parts; part++ {
			reqOptions.Params["part"] = strconv.Itoa(part)
			errTimeout := time.Duration(0)
			for {
				resp, err := grequests.Get(baseURL+".csv", reqOptions)
				log.Debug().Msgf("GET URL=%v Params=%v", baseURL+".csv", reqOptions.Params)
				if err != nil || !resp.Ok {
					if errTimeout < 30*time.Second {
						log.Warn().Int("response_status", resp.StatusCode).Str("response_body", resp.String()).Err(err).Msgf("GET %s Response don't return data", baseURL+".csv")
						errTimeout += time.Second
						time.Sleep(errTimeout)
					} else {
						log.Error().Int("response_status", resp.StatusCode).Str("response_body", resp.String()).Int("errTimeout", int(errTimeout)).Err(err).Msgf("GET %s Response don't return data timeout exceed", baseURL+".csv")
						break
					}

				}
				if resp.StatusCode == 202 {
					log.Warn().Int("response_status", resp.StatusCode).Str("response_body", resp.String()).Int("errTimeout", int(errTimeout)).Err(err).Msgf("GET %s in progress", baseURL+".csv")
					errTimeout += time.Second
					time.Sleep(errTimeout)
				}
				if resp.StatusCode == 200 {
					log.Info().Int("response_status", resp.StatusCode).Err(err).Msgf("GET %s DONE", baseURL+".csv")
					d.dispatchData.Add(1)
					go d.dispatchCSVResponse(fullTable, resp, reqOptions, sqlFields, applicationID, baseURL+".csv")
					break
				}
			}
		}
		return true, nil
	}
}

func (d *DataLoader) metricaLoadData(fullTable string, shortTable string, applicationID string) pool.WorkFunc {

	return func(wu pool.WorkUnit) (interface{}, error) {
		defer log.Info().Msgf("metricaLoadData %s end", fullTable)
		log.Info().Msgf("metricaLoadData %s begin", fullTable)
		if wu.IsCancelled() {
			return nil, nil
		}
		metrikaEvolution := logRequestEvaluationJSON{}
		evaluteURL := fmt.Sprintf("%s/management/v1/counter/%s/logrequests/evaluate", basecfg.MetrikaURL, applicationID)
		reqFields, sqlFields := d.prepareFieldsToRequest(fullTable)
		dateSince, dateUntil := d.prepareDateRange(fullTable, applicationID)
		if dateSince.Format("2006-01-02") >= dateUntil.Format("2006-01-02") {
			return nil, fmt.Errorf("dateSince=%s >= dateUntil=%s", dateSince, dateUntil)
		}

		reqOptions := grequests.RequestOptions{
			Params: map[string]string{
				"oauth_token": d.config.OAuth.Token,
				"date1":       dateSince.Format("2006-01-02"),
				"date2":       dateUntil.Format("2006-01-02"),
				"source":      shortTable,
				"fields":      reqFields,
			},
		}
		resp, err := grequests.Get(evaluteURL, &reqOptions)
		d.processMetrikaResponse(evaluteURL, "GET", reqOptions, err, resp, &metrikaEvolution)
		if !metrikaEvolution.LogRequestEvaluation.Possible {
			for date := dateSince; date.Before(dateUntil); date = date.AddDate(0, 0, int(metrikaEvolution.LogRequestEvaluation.MaxPossibleDayQuantity)) {
				endDate := date.AddDate(0, 0, int(metrikaEvolution.LogRequestEvaluation.MaxPossibleDayQuantity))
				if endDate.After(dateUntil) {
					endDate = dateUntil
				}
				d.processMetrikaLogRequest(date, endDate, reqFields, sqlFields, shortTable, fullTable, applicationID)
			}
		} else {
			d.processMetrikaLogRequest(dateSince, dateUntil, reqFields, sqlFields, shortTable, fullTable, applicationID)
		}

		return true, nil
	}
}

func (d *DataLoader) processMetrikaLogRequest(date1 time.Time, date2 time.Time, reqFields string, sqlFields string, shortTable string, fullTable string, applicationID string) {
	logRequestStatus := logRequestJSON{}
	d.createMetrikaLogRequest(applicationID, date1, date2, shortTable, reqFields, &logRequestStatus)
	d.checkMertikaLogRequestStatus(&logRequestStatus, applicationID)
	d.downloadPartsMetrikaLogRequest(&logRequestStatus, applicationID, fullTable, sqlFields)
}

func (d *DataLoader) createMetrikaLogRequest(applicationID string, date1 time.Time, date2 time.Time, shortTable string, fields string, logRequestStatus *logRequestJSON) {
	createURL := fmt.Sprintf("%s/management/v1/counter/%s/logrequests", basecfg.MetrikaURL, applicationID)
	reqOptions := grequests.RequestOptions{
		Params: map[string]string{
			"oauth_token": d.config.OAuth.Token,
			"date1":       date1.Format("2006-01-02"),
			"date2":       date2.Format("2006-01-02"),
			"source":      shortTable,
			"fields":      fields,
		},
	}
	resp, err := grequests.Post(createURL, &reqOptions)
	d.processMetrikaResponse(createURL, "POST", reqOptions, err, resp, &logRequestStatus)
}

func (d *DataLoader) checkMertikaLogRequestStatus(logRequestStatus *logRequestJSON, applicationID string) {
	badLogRequestStatus := map[string]bool{
		"processing_failed":                true,
		"cleaned_by_user":                  true,
		"cleaned_automatically_as_too_old": true,
	}
	retryCount := 0
	log.Debug().Str("logRequestStatus.LogRequest.Status", logRequestStatus.LogRequest.Status).Msg("checkMertikaLogRequestStatus")
	for logRequestStatus.LogRequest.Status != "processed" {
		if _, badExists := badLogRequestStatus[logRequestStatus.LogRequest.Status]; badExists {
			log.Fatal().Str("log_request_status", logRequestStatus.LogRequest.Status).Msg("Bad log request status")
		}
		time.Sleep(time.Second * 20)
		checkURL := fmt.Sprintf("%s/management/v1/counter/%s/logrequest/%d", basecfg.MetrikaURL, applicationID, logRequestStatus.LogRequest.RequestID)
		reqOptions := grequests.RequestOptions{
			Params: map[string]string{
				"oauth_token": d.config.OAuth.Token,
			},
		}
		resp, err := grequests.Get(checkURL, &reqOptions)
		d.processMetrikaResponse(checkURL, "GET", reqOptions, err, resp, &logRequestStatus)
		retryCount++
		if retryCount == 100 && logRequestStatus.LogRequest.Status != "processed" {
			log.Fatal().Msgf("GET %s Too many retries with status=%s", checkURL, logRequestStatus.LogRequest.Status)
		}
		log.Debug().Str("logRequestStatus.LogRequest.Status", logRequestStatus.LogRequest.Status).Msg("checkMertikaLogRequestStatus")
	}
}

func (d *DataLoader) downloadPartsMetrikaLogRequest(logRequestStatus *logRequestJSON, applicationID string, fullTable string, sqlFields string) {
	for _, part := range logRequestStatus.LogRequest.Parts {
		downloadURL := fmt.Sprintf("%s/management/v1/counter/%s/logrequest/%d/part/%d/download", basecfg.MetrikaURL, applicationID, logRequestStatus.LogRequest.RequestID, part.PartNumber)
		reqOptions := grequests.RequestOptions{
			Params: map[string]string{
				"oauth_token": d.config.OAuth.Token,
				"part":        strconv.Itoa(part.PartNumber),
			},
			Headers: map[string]string{
				"Accept-Encoding": "gzip",
			},
		}
		resp, err := grequests.Get(downloadURL, &reqOptions)
		d.processMetrikaResponse(downloadURL, "GET", reqOptions, err, resp, nil)
		d.dispatchData.Add(1)
		go d.dispatchTSVResponse(fullTable, resp, reqOptions, sqlFields, applicationID, downloadURL)
	}
}

func (d *DataLoader) processMetrikaResponse(url string, method string, reqOptions grequests.RequestOptions, err error, resp *grequests.Response, jsonData interface{}) {
	log.Debug().Msgf("%s URL=%v Params=%v", method, url, reqOptions.Params)
	if err != nil || !resp.Ok {
		log.Fatal().Int("response_status", resp.StatusCode).Str("response_body", resp.String()).Err(err).Msgf("%s Response error", url)
	}
	if jsonData != nil {
		if err = resp.JSON(jsonData); err != nil {
			log.Fatal().Int("response_status", resp.StatusCode).Str("response_body", resp.String()).Err(err).Msg("JSON Parsing error")
		}
	}
}

func (d *DataLoader) prepareDateRange(fullTable string, applicationID string) (dateSince time.Time, dateUntil time.Time) {
	dateSince = d.config.DateSince
	dateUntil = d.config.DateUntil
	table := d.config.Clickhouse.TablePrefix + fullTable
	if d.config.Clickhouse.UseDistributed {
		table += "_distributed"
	} else {
		table += d.config.Clickhouse.TableSuffix
	}

	if !d.config.Clickhouse.DropTable {
		appIDField := "CounterID"
		if fullTable[:10] == "appmetrika" {
			appIDField = "app_id"
		}
		timeField := TableDefinitions[fullTable]["key_definition"]["time_field"]
		q := chpool.FormatSQLTemplate(
			"SELECT MAX({timeField}) AS dateSince FROM {db}.{table} WHERE {appIDField}=? AND {timeField} >= toDateTime(?) AND {timeField} <= toDateTime(?)",
			map[string]interface{}{
				"db":         d.config.Clickhouse.Database,
				"table":      table,
				"timeField":  timeField,
				"appIDField": appIDField,
			},
		)
		db := chpool.GetRandomClickhouseConnection()
		appID, _ := strconv.Atoi(applicationID)
		rows, err := db.Query(q, appID, dateSince.Unix(), dateUntil.Unix())
		if err != nil {
			log.Fatal().Str("q", q).Time("dateSince", dateSince).Time("dateUntil", dateUntil).Int("appID", appID).Err(err).Msg("prepareDateRange Query Error")
		}
		cols, _ := rows.Columns()
		for rows.Next() {
			r, err := chpool.FetchRowAsMap(rows, cols)
			if err != nil {
				log.Fatal().Str("q", q).Time("dateSince", dateSince).Time("dateUntil", dateUntil).Int("appID", appID).Err(err).Msg("prepareDateRange Fetch Error")
			}
			dateSince = r["dateSince"].(time.Time)
		}
	}
	log.Debug().Time("dateSince", dateSince).Time("dateUntil", dateUntil).Msg("PreapareDateRange DONE")
	return dateSince, dateUntil
}

func (d *DataLoader) prepareFieldsToRequest(table string) (reqFields string, sqlFields string) {
	fieldCount := len(TableDefinitions[table]["field_types"])
	fieldNames := make([]string, fieldCount)
	i := 0
	for fieldName, fieldType := range TableDefinitions[table]["field_types"] {
		if !strings.Contains(fieldType, "MATERIALIZED") {
			fieldNames[i] = fieldName
			i++
		}
	}
	fieldCount = i
	fieldNames = fieldNames[0:fieldCount]
	sort.Strings(fieldNames)
	for i, fieldName := range fieldNames {
		// Metrika Name Conversion
		if lastIndex := strings.LastIndex(fieldName, ":"); lastIndex != -1 {
			sqlFields += strings.Title(fieldName[lastIndex+1:])
		} else {
			// AppMetrika Name Conversion
			sqlFields += fieldName
		}
		reqFields += fieldName
		if i < fieldCount-1 {
			sqlFields += ","
			reqFields += ","
		}
	}
	return reqFields, sqlFields
}

func (d *DataLoader) dispatchTSVResponse(table string, response *grequests.Response, reqOptions grequests.RequestOptions, sqlFields, applicationID, downloadURL string) {
	defer d.dispatchData.Done()

	tsvFileName := path.Join(d.config.TmpDir, applicationID+"."+table+"."+reqOptions.Params["part"]+".tsv")
	gzFileName := tsvFileName + ".gz"
	if err := response.DownloadToFile(gzFileName); err != nil {
		log.Fatal().Err(err).Str("gzFileName", gzFileName).Str("downloadURL", downloadURL).Msg("response.DownloadToFile error")
	}

	/* @TODO #3:10m/DEV remove exec after resolve https://gist.github.com/Slach/dbbc874bf102df469d7daed326dc5456 */
	helpers.ExecShellCmd(fmt.Sprintf("gzip -cd %s | sed %s > %s", gzFileName, "\"s/\\\\\\\\'/'/g;s/\\\"\\['/['/g;s/'\\]\\\"/']/g\"", tsvFileName), "Unpack error")
	helpers.ExecShellCmd(fmt.Sprintf("gzip -f %s", tsvFileName), "Gzip error")

	chpool.LoadGzipFileToTable(&d.config.ConfigType, gzFileName, table, "TabSeparatedWithNames", sqlFields)
}

func (d *DataLoader) dispatchCSVResponse(table string, response *grequests.Response, reqOptions *grequests.RequestOptions, sqlFields, applicationID, downloadURL string) {
	defer d.dispatchData.Done()

	csvFileName := path.Join(d.config.TmpDir, applicationID+"."+table+"."+reqOptions.Params["part"]+".csv")
	gzFileName := csvFileName + ".gz"
	if err := response.DownloadToFile(gzFileName); err != nil {
		log.Fatal().Err(err).Str("gzFileName", gzFileName).Str("downloadURL", downloadURL).Msg("response.DownloadToFile error")
	}
	/* @TODO #4:10m/DEV remove exec after Yandex AppMetrika Team add application_id to CSV output */
	helpers.ExecShellCmd(fmt.Sprintf("gzip -cd %s | head -n 1 | sed %s > %s", gzFileName, "\"s/$/,app_id/\"", csvFileName), "Head unpack error")
	helpers.ExecShellCmd(fmt.Sprintf("gzip -cd %s | tail -n +2 | sed %s >> %s", gzFileName, "\"s/$/,"+applicationID+"/\"", csvFileName), "Body unpack error")
	helpers.ExecShellCmd(fmt.Sprintf("gzip -f %s", csvFileName), "Gzip error")
	chpool.LoadGzipFileToTable(&d.config.ConfigType, gzFileName, table, "CSVWithNames", sqlFields+",app_id")
}
